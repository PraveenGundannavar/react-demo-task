import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from '../../redux/reducer';
import { Link } from "react-router-dom";

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {
        // let {email,password} = this.state
        let { isLoginPending, isLoginSuccess, loginError } = this.props
        return (
            <div className="auth-wrapper" onSubmit={this.onSubmit}>
                <div className="auth-inner">
                    <form name="Login">

                        <h3>Sign In</h3>
                        <div className="text-center">
                            {isLoginPending && <div>Please wait ...</div>}
                            {isLoginSuccess && <div className="success">{this.props.history.push("/meeting")}</div>}
                            {loginError && <div className="warning">Invalid email or password</div>}
                        </div>

                        <div className="form-group">
                            <label>Email address</label>
                            <input type="email" className="form-control" name="email" placeholder="Enter email" onChange={e => this.setState({ email: e.target.value })} />
                        </div>

                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" className="form-control" name="password" placeholder="Enter password" onChange={e => this.setState({ password: e.target.value })} />
                        </div>

                        <div className="form-group">
                        <button type="submit" className="btn btn-primary btn-block">Submit</button>
                        </div>

                        <hr></hr>
                        <div>
                            <label>Login Credentils</label><br></br>
                            <label>Email : admin@gmail.com</label><br></br>
                            <label>password : 123456</label>
                        </div>

                    </form>
                </div>
            </div>
        )
    }
    onSubmit = (e) => {
        e.preventDefault();

        let { email, password } = this.state
        this.props.login(email, password);
    }
}



const mapStateToProps = (state) => {
    return {
        isLoginPending: state.isLoginPending,
        isLoginSuccess: state.isLoginSuccess,
        loginError: state.loginError

    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: (email, password) => dispatch(login(email, password))

    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
